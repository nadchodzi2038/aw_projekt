#pragma once

// *Really* minimal PCG32 code / (c) 2014 M.E. O'Neill / pcg-random.org
// Licensed under Apache License 2.0 (NO WARRANTY, etc. see website)

typedef struct { unsigned long long state; unsigned long long inc; } pcg32_random_t;

inline unsigned pcg32_random_r(pcg32_random_t& rng)
{
	unsigned long long oldstate = rng.state;
	// Advance internal state
	rng.state = oldstate * 6364136223846793005ULL + (rng.inc|1);
	// Calculate output function (XSH RR), uses old state for max ILP
	unsigned xorshifted = ((oldstate >> 18u) ^ oldstate) >> 27u;
	unsigned rot = oldstate >> 59u;
	return (xorshifted >> rot) | (xorshifted << ((-rot) & 31));
}