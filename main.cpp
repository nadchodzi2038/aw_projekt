#include "common.hpp"
#include "sequential_merge_sort.hpp"
#include <iostream>
#include <string>
#include <omp.h>
#include <algorithm>
#include <stdexcept>
#include "basic_parallel_merge_sort.hpp"
#include "bench.hpp"

std::vector<value_type> read_data(std::istream& input_stream)
{
	std::istream_iterator<int> b(input_stream);
	std::istream_iterator<int> e;
	std::vector<int> result;
	std::copy(b, e, std::back_inserter(result));
	return result;
}

std::vector<value_type> read_input(std::istream& is)
{
	char type;
	is >> type;
	if(!is)
	{
		throw std::invalid_argument("bad input data");
	}
	if(type == 'R')
	{
		std::size_t input_size_in_bytes;
		unsigned long long seed;
		if(is >> input_size_in_bytes >> seed)
		{
			return generate_data(input_size_in_bytes, seed);
		}
		else
		{
			throw std::invalid_argument("bad input data");
		}
	}
	else if(type == 'L')
	{
		return read_data(is);
	}
	else
	{
		throw std::invalid_argument("bad input data");
	}
}

void parallel(std::vector<value_type>& input)
{
	basic_parallel_merge_sort(input.data(), input.data() + input.size());
}

void sequential(std::vector<value_type>& input)
{
	sequential_merge_sort(input.data(), input.data() + input.size());
}

int main()
{
	std::vector<value_type> data = read_input(std::cin);
	std::vector<BenchmarkResults> benchmarkResults(2);
	{
		std::vector<value_type> sorted = data;
		benchmarkResults[0] = mark("basic parallel multiple threads", parallel, sorted);
		std::cout << "Sorted? " << (is_sorted(sorted.begin(), sorted.end()) ? "YES!" : "NO") << "\n";
	}
	{
		std::vector<value_type> sorted = data;
		benchmarkResults[1] = mark("sequential", sequential, sorted);
		std::cout << "Sorted? " << (is_sorted(sorted.begin(), sorted.end()) ? "YES!" : "NO") << "\n";
	}
	for(std::vector<BenchmarkResults>::iterator it = benchmarkResults.begin(), end = benchmarkResults.end(); it != end; ++it)
	{
		std::cout << *it << "\n";
	}
	std::cout << speedup_uncertainty(benchmarkResults[0], benchmarkResults[1]) << "\n";
}