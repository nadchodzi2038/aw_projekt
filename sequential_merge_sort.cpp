#include "sequential_merge_sort.hpp"
#include <vector>

namespace detail
{
	void sequential_merge(iterator src, std::size_t b, std::size_t m, std::size_t e, iterator dst)
	{
		std::size_t i = b;
		std::size_t j = m;

		// While there are elements in the left or right runs...
		for(std::size_t k = b; k < e; k++)
		{
			// If left run head exists and is <= existing right run head.
			if(i < m && (j >= e || compare(src[i], src[j])))
			{
				dst[k] = src[i];
				++i;
			}
			else
			{
				dst[k] = src[j];
				++j;
			}
		}
		std::copy(dst + b, dst + e, src + b);
	}
}

void sequential_merge_sort(iterator src, std::size_t b, std::size_t e, iterator dst)
{
	if(e - b < 2)
		return;

	std::size_t m = (e + b) / 2;

	sequential_merge_sort(src, b, m, dst);
	sequential_merge_sort(src, m, e, dst);

	detail::sequential_merge(src, b, m, e, dst);
}

void sequential_merge_sort(iterator begin, iterator end)
{
	std::vector<value_type> input(begin, end);
	sequential_merge_sort(input.data(), 0, input.size(), begin);
}

#include <cassert>

namespace
{
	struct test_basic_parallel
	{
		test_basic_parallel()
		{
			{
				value_type actual[] = {6, 5, 8, 2, 5, 7, 4};
				sequential_merge_sort(actual, actual + countof(actual));
				value_type expected[] = {2, 4, 5, 5, 6, 7, 8};
				assert(std::equal(std::begin(actual), std::end(actual), std::begin(expected), std::end(expected)));
			}
			{
				int a_init[] = {1, 8, 2};
				std::vector<int> a = from_c_array(a_init);
				std::vector<int> b = a;
				detail::sequential_merge(a.data(), 0, 2, a.size(), b.data());
				int c_init[] = {1, 2, 8};
				std::vector<int> c = from_c_array(c_init);
				assert(std::equal(std::begin(b), std::end(b), std::begin(c), std::end(c)));
			}
			{
				int a_init[] = {1, 3, 5, 7, 2, 4, 6};
				std::vector<int> a = from_c_array(a_init);
				std::vector<int> b = a;
				detail::sequential_merge(a.data(), 0, 4, a.size(), b.data());
				int c_init[] = {1, 2, 3, 4, 5, 6, 7};
				std::vector<int> c = from_c_array(c_init);
				assert(std::equal(std::begin(b), std::end(b), std::begin(c), std::end(c)));
			}
			{
				int a_init[] = {1, 3, 5, 2, 4, 6};
				std::vector<int> a = from_c_array(a_init);
				std::vector<int> b = a;
				detail::sequential_merge(a.data(), 0, 3, a.size(), b.data());
				int c_init[] = {1, 2, 3, 4, 5, 6};
				std::vector<int> c = from_c_array(c_init);
				assert(std::equal(std::begin(b), std::end(b), std::begin(c), std::end(c)));
			}
		}
	} test_basic_parallel;
}