cmake_minimum_required(VERSION 2.8)

project(zzz)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++98 -Wno-long-long -O3")

if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -pedantic -fopenmp")
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel")
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mmic -openmp")
endif ()

add_executable(vectorized
		common.hpp
		main.cpp
		sequential_merge_sort.cpp
		basic_parallel_merge_sort.cpp bench.hpp pcg-random.hpp)