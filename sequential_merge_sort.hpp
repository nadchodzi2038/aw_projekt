#pragma once
#include "common.hpp"

void sequential_merge_sort(iterator begin, iterator end);
void sequential_merge_sort(iterator src, std::size_t b, std::size_t e, iterator dst);