#include "basic_parallel_merge_sort.hpp"
#include "sequential_merge_sort.hpp"
#include <vector>
#include <omp.h>
#include <iostream>

namespace detail
{
	void basic_parallel_merge(iterator src, std::size_t b, std::size_t m, std::size_t e, iterator dst)
	{
		std::size_t i = b;
		std::size_t j = m;

		for(std::size_t k = b; k < e; k++)
		{
			if(i < m && (j >= e || compare(src[i], src[j])))
			{
				dst[k] = src[i];
				++i;
			}
			else
			{
				dst[k] = src[j];
				++j;
			}
		}
		std::copy(dst + b, dst + e, src + b);
	}

	void basic_parallel_merge_sort(iterator src, std::size_t b, std::size_t e, iterator dst, unsigned thread_count)
	{
		if(e - b < 2)
			return;

		if(thread_count == 1)
		{
			sequential_merge_sort(src, b, e, dst);
			return;
		}

		std::size_t m = (e + b) / 2;
		#pragma omp parallel sections num_threads(2)
		{
			#pragma omp section
			{
				basic_parallel_merge_sort(src, b, m, dst, thread_count/2);
			}
			#pragma omp section
			{
				basic_parallel_merge_sort(src, m, e, dst, thread_count/2);
			}
		}

		basic_parallel_merge(src, b, m, e, dst);
	}
}

void basic_parallel_merge_sort(iterator begin, iterator end, unsigned int thread_count)
{
	std::vector<value_type> input(begin, end);
	omp_set_num_threads(thread_count);
	omp_set_nested(1);
	detail::basic_parallel_merge_sort(input.data(), 0, input.size(), begin, thread_count);
}

void basic_parallel_merge_sort(iterator begin, iterator end)
{
	return basic_parallel_merge_sort(begin, end, omp_get_num_procs());
}

#include <cassert>

namespace
{
	struct test_basic_parallel
	{
		test_basic_parallel()
		{
			{
				std::vector<int> actual = generate_data(100000, 0);
				std::vector<int> expected = actual;
				std::sort(expected.begin(), expected.end());
				basic_parallel_merge_sort(actual.data(), actual.data() + actual.size());
				assert(std::equal(std::begin(actual), std::end(actual), std::begin(expected), std::end(expected)));
			}
			{
				int a_init[] = { 1, 8, 2 };
				std::vector<int> a = from_c_array(a_init);
				std::vector<int> b = a;
				detail::basic_parallel_merge(a.data(), 0, 2, a.size(), b.data());
				int c_init[] = { 1, 2, 8 };
				std::vector<int> c = from_c_array(c_init);
				assert(std::equal(std::begin(b), std::end(b), std::begin(c), std::end(c)));
			}
			{
				int a_init[] = { 1, 3, 5, 7, 2, 4, 6 };
				std::vector<int> a = from_c_array(a_init);
				std::vector<int> b = a;
				detail::basic_parallel_merge(a.data(), 0, 4, a.size(), b.data());
				int c_init[] = { 1, 2, 3, 4, 5, 6, 7 };
				std::vector<int> c = from_c_array(c_init);
				assert(std::equal(std::begin(b), std::end(b), std::begin(c), std::end(c)));
			}
			{
				int a_init[] = { 1, 3, 5, 2, 4, 6 };
				std::vector<int> a = from_c_array(a_init);
				std::vector<int> b = a;
				detail::basic_parallel_merge(a.data(), 0, 3, a.size(), b.data());
				int c_init[] = { 1, 2, 3, 4, 5, 6 };
				std::vector<int> c = from_c_array(c_init);
				assert(std::equal(std::begin(b), std::end(b), std::begin(c), std::end(c)));
			}
		}
	} test_basic_parallel;
}