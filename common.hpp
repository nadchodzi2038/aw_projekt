#pragma once
#include <iterator>
#include <functional>
#include <algorithm>
#include <vector>
#include "pcg-random.hpp"

typedef int* iterator;
typedef int value_type;

inline bool compare(value_type l, value_type r)
{
	return l <= r;
}

inline std::vector<value_type> generate_data(std::size_t size_in_bytes, unsigned long long seed)
{
	pcg32_random_t gen = { seed, 1 };
	std::size_t size = size_in_bytes / sizeof(value_type);
	std::vector<value_type> result;
	result.reserve(size);
	for(std::size_t i = 0; i < size; ++i)
	{
		result.push_back(pcg32_random_r(gen));
	}
	return result;
}

template <typename T, size_t N>
char ( &countof_helper( T (&array)[N] ))[N];
#define countof( array ) (sizeof( countof_helper( array ) ))

template<typename T, size_t N>
std::vector<T> from_c_array(T (&array)[N])
{
	return std::vector<T>(array + 0, array + N);
}

template<class ForwardIt>
ForwardIt is_sorted_until(ForwardIt first, ForwardIt last)
{
	if (first != last) {
		ForwardIt next = first;
		while (++next != last) {
			if (*next < *first)
				return next;
			first = next;
		}
	}
	return last;
}

template<class ForwardIt>
bool is_sorted(ForwardIt first, ForwardIt last)
{
	return is_sorted_until(first, last) == last;
}