#pragma once
#include "common.hpp"

void basic_parallel_merge_sort(iterator begin, iterator end);
void basic_parallel_merge_sort(iterator begin, iterator end, unsigned thread_count);
