#pragma once
#include <iostream>
#include <omp.h>
#include <vector>
#include <numeric>
#include <cmath>

struct BenchmarkResults
{
	std::string benchmark_name;
	double min;
	double max;
	double mean;
	double uncertainty;

	friend std::ostream& operator<<(std::ostream& os, const BenchmarkResults& results)
	{
		os << "Executed " << results.benchmark_name << " in (mean): " << results.mean << " seconds . Uncertainty is " << results.uncertainty << " seconds\n";
		return os;
	}
};

double squared(double x)
{
	return x * x;
}

double speedup_uncertainty(BenchmarkResults slower, BenchmarkResults faster)
{
	double t1 = slower.mean;
	double t2 = faster.mean;
	double delta_t1 = slower.uncertainty;
	double delta_t2 = faster.uncertainty;
	return std::abs(t1/t2) * std::sqrt(squared(delta_t1/t1) + squared(delta_t2/t2));
}

template<typename Func, typename Param>
BenchmarkResults mark(const std::string& name, Func f, Param& p)
{
	std::vector<double> measurements(20);
	for(std::size_t i = 0; i < measurements.size(); ++i)
	{
		const double before = omp_get_wtime();
		f(p);
		const double after = omp_get_wtime();
		measurements[i] = after - before;
	}
	BenchmarkResults results;
	results.benchmark_name = name;
	results.min = *std::min_element(measurements.begin(), measurements.end());
	results.max = *std::max_element(measurements.begin(), measurements.end());
	results.mean = std::accumulate(measurements.begin(), measurements.end(), 0.0)/measurements.size();
	double divisor = 2 * std::sqrt(measurements.size());
	results.uncertainty = (results.max - results.min)/divisor;
	return results;
}